import React,{Component} from "react";
import {Row,Col} from 'antd';
import {connect} from "react-redux";
import Profile from './Profile'
import Cart from "./Cart";
class SiteHeader extends  React.Component{
    constructor(props) {
        super();
    }
    render(){
        return (
        <Row>
            <Col span={16}>Empty or header image or search element </Col>
            <Col span={6}><Cart /></Col>
            <Col span={2}><Profile /></Col>
        </Row>
        )
    }
}

function mapDispatchToProps(dispatch){
    return {
        filelistrecord: (curRec) => dispatch({type: "USER_INTERACTION", record: curRec})
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(SiteHeader);