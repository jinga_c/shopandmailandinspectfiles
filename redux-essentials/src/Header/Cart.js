import React,{Component} from "react";
import {Row,Col, Button, Card, Popover} from 'antd';
import {connect} from "react-redux";
import {UserOutlined,ShoppingCartOutlined } from '@ant-design/icons';


class Cart extends  React.Component{
    constructor(props) {
        super();
    }
    handleOnClick = (e) => {

    }
    content = () =>{
        return (
            <div>
                <Card>
                    Shopping Card Contents
                </Card>
            </div>
        )
    }
    render(){
        return (
            <Card style={{border: 'none'}}>
                <Popover content={this.content} title="Items in card" trigger="click">
                    <Button onClick={this.handleOnClick}><ShoppingCartOutlined />5 products $55</Button>
                </Popover>
            </Card>
        )
    }
}

function mapDispatchToProps(dispatch){
    return {
        filelistrecord: (curRec) => dispatch({type: "USER_INTERACTION", record: curRec})
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(Cart);