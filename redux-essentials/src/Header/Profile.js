import React,{Component} from "react";
import {Row,Col, Button, Card, Dropdown, Menu, Popover} from 'antd';
import {connect} from "react-redux";
import {UserOutlined} from '@ant-design/icons';
import {useState} from "react";


class Profile extends  React.Component{
    constructor(props) {
        super();
        this.state = {
            contextMenu: false,
            zIndex: 0,
        }
        this.wrapperRef = React.createRef();
        this.handleClickOutside = this.handleClickOutside.bind(this);


    }
    handleOnClick = (e) => {
        this.setState( { contextMenu : !this.state.contextMenu})
        if(this.state.contextMenu) this.setState({zIndex: '10'})
        if(!this.state.contextMenu) this.setState({zIndex: '0'})
        console.log(this.state.contextMenu)

    }
    componentDidMount() {
        document.addEventListener("mousedown", this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClickOutside);
    }
    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.current.contains(event.target)) {
            this.setState({contextMenu: false})
        }
    }
    render(){

        return (
            <Card style={{border: 'none', width: '120%'}} ref={this.wrapperRef}>
                <Button block onClick={this.handleOnClick}><UserOutlined />John Doe
                </Button>
                <div style={{display: 'block', visibility: this.state.contextMenu ? 'visible': 'hidden'}}>
                <Button block style={{zIndex: 10, borderBottom: 'none', borderTop: 'none'}}>Profile</Button>
                <Button block style={{zIndex: 10,  borderBottom: 'none', borderTop: 'none'}}>Api settings</Button>
                <Button block style={{zIndex: 10, borderTop: 'none'}}>Payments</Button>
                </div>

            </Card>
        )
    }

}

function mapDispatchToProps(dispatch){
    return {
        ProfileService: (curRec) => dispatch({type: "USER_PROFILE", record: curRec})
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(Profile);