import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavMenu from "./features/navigation/NavMenu";
import {Menu, Button,Col,Row, Layout} from 'antd';
import 'antd/dist/antd.css';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import {Link,Route, Routes, Router} from 'react-router-dom';
import UploadFiles from './Presentational/UploadFile'
import Uploads from './Presentational/Uploads'
import FilesList from "./Presentational/FilesList";
import Statistics from "./Presentational/Statistics";
import SiteHeader from "./Header/Header";
import Products from "./Presentational/Products";

const { Header, Footer, Sider, Content } = Layout;

function App() {

  return (
    <Layout hasSider>
        <Sider style={{height: '100vh', background: "white", textAlign: 'center'}}>
            <NavMenu />
        </Sider>
        <Layout >
            <Header style={{background: 'white',  textAlign: 'center' }}><SiteHeader /></Header>
            <Content style={{background: 'white',  textAlign: 'center' }}>
                <Routes>
                    <Route path="fileslist" element={<FilesList />} />
                    <Route path="statistics" element={<Statistics />}/>
                    <Route path="upload"  element={<Uploads />} />
                    <Route path="products" element={<Products />} />
                </Routes>
            </Content>
            <Footer style={{background: 'white',  textAlign: 'center' }}>Footer</Footer>
        </Layout>

    </Layout>
  );
}

export default App;
