import {connect} from "react-redux";
import React,{Component} from 'react';
import {Table,Tag,Space, Modal, Popover, Button} from 'antd';

/*Get a list of uploaded files */

class FilesList extends Component {
    constructor(props){
        super(props);
        this.state = {
            items:[],
            errors:[],
            modalVisible: false,

        }
        console.log(this.state.items)
        this.showModal = this.showModal.bind(this)
        this.handleOk = this.handleOk.bind(this)
        this.handleCancel = this.handleCancel.bind(this)
        this.requestAntivAttrib = this.requestAntivAttrib.bind(this)
    }
    componentDidMount() {
        let data_url = "http://localhost:5000/getfilelist"

        fetch(data_url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        items: result
                    })
                },
                (error) =>{
                    this.setState({
                        errors:error,
                    })
                })
    }
    showModal(){
        this.setState({
            modalVisible: true
        })
        console.log(this.state.modalVisible)
    }
    handleOk(){
        this.setState({
            modalVisible: false
        })
    }
    handleCancel(){
        this.setState({
            modalVisible: false
        })
    }
    requestAntivAttrib(obj){
        console.log(obj.times_submitted)
        /*fetch("http://localhost:5000",{
            method: 'POST',
            mode: "same-origin",
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj)
        })*/
    }
    render(){
        const columns = [
            {
                title: 'Filename',
                dataIndex: 'name',
                key: 'name',
                /*render: text => <a>{text}</a>,*/
            },
            {
                title: 'Mimetype',
                dataIndex: 'mimetype',
                key: 'mimetype',
            },
            {
                title: 'Filesize',
                dataIndex: 'fsz_bytes',
                key: 'filesize',
            },
            {
                title: 'SubmittedFile',
                key: 'submitted_file',
                dataIndex: 'submitted_file',
                render: tags => (
                    <>
                        {tags.map(tag => {
                            let date_sub = new Date(tag.date)

                            let content = (
                                <div>
                                    <p>date submitted: {date_sub.toUTCString()}</p>
                                    <p>md5: {tag.md5}</p>
                                    <p>sha1: {tag.sha1}</p>
                                    <p>sha256: {tag.sha256}</p>
                                    <p>size: {tag.size}</p>
                                </div>
                            )
                            let color = tag.length > 5 ? 'geekblue' : 'green';
                            if (tag === 'loser') {
                                color = 'volcano';
                            }
                            let mydate=new Date(tag.date)
                            return (
                                <>
                                <Tag color={color} key={tag.id}>
                                    Queued: {tag.status ? "yes": "no"}
                                </Tag>

                                    <Popover content={content} title="File details">
                                        <Button type="primary">File details</Button>
                                    </Popover>
                                </>
                            );
                        })}
                    </>
                ),
            },
            {
                title: 'Antivirus Response',
                key: 'antiv_response',
                dataIndex: 'antiv_response',
                render: tags => (
                    <>
                        {tags.map(tag => {
                            let color = tag.length > 5 ? 'geekblue' : 'green';
                            if (tag === 'loser') {
                                color = 'volcano';
                            }
                            const last_analysis_stats = (
                                <div>
                                    <p>Confirmed-timeout: {tag.attributes.last_analysis_stats['confirmed-timeout']}</p>
                                    <p>Failure: {tag.attributes.last_analysis_stats['failure']}</p>
                                    <p>Harmless: {tag.attributes.last_analysis_stats['harmless']}</p>
                                    <p>Malicious: {tag.attributes.last_analysis_stats['malicious']}</p>
                                    <p>Suspicious: {tag.attributes.last_analysis_stats['suspicious']}</p>
                                    <p>Timeout: {tag.attributes.last_analysis_stats['timeout']}</p>
                                    <p>Type-unsupported: {tag.attributes.last_analysis_stats['type-unsupported']}</p>
                                    <p>Undetected: {tag.attributes.last_analysis_stats['undetected']}</p>

                                </div>
                            )
                            let mydate=new Date(tag.date)
                            return (
                                <>
                                    <Tag color={color} key={tag.id}>
                                        Received: {tag.obj_id ? 'Yes' : 'No'}
                                        <br />

                                    <Popover content={last_analysis_stats} title="Antivirus analysis stats">
                                        <Button type="primary">Show response</Button>
                                    </Popover>
                                    </Tag>
                                </>
                            );
                        })}
                    </>
                ),
            },
            {
                title: 'Operations',
                key: 'operations',
                dataIndex: 'operations',

                render: tags => (
                    <>
                        {tags.map(tag => {
                            let color = tag.length > 5 ? 'geekblue' : 'green';
                            if (tag === 'loser') {
                                color = 'volcano';
                            }
                            let content = (
                                <div>
                                    <p>operationid : {tag.op_id}</p>
                                </div>
                            )
                            let mydate=new Date(tag.date)
                            return (
                                <>
                                    <Tag color={color} key={tag.id}>
                                        processed: {tag.op_id ? 'yes': 'no'}
                                    </Tag>
                                    <Popover content={content} title="Operation details">
                                        <Button type="primary">Show details</Button>
                                    </Popover>

                                </>
                            );
                        })}
                    </>
                ),
            }
            /*{
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <Space size="middle">
                        <a>Invite {record.name}</a>
                        <a>Delete</a>
                    </Space>
                ),
            },*/
        ];

        let rows = this.state.items;
        return (
            /*<>
            </>*/
            <Table rowKey={rows => rows.id} columns={columns} dataSource={rows} onRow={(record, rowIndex) => {
                return {
                    onClick: event => {this.props.filelistrecord(record)}, // click row
                    onDoubleClick: event => {}, // double click row
                    onContextMenu: event => {console.log(record)}, // right button click row
                    onMouseEnter: event => {}, // mouse enter row
                    onMouseLeave: event => {}, // mouse leave row
                }}}
            />
        )
    }
}


function mapDispatchToProps(dispatch){
    return {
        filelistrecord: (curRec) => dispatch({type: "USER_INTERACTION", record: curRec})
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(FilesList);
