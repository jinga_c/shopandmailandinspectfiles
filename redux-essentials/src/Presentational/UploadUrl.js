import {  Upload, message, Button, Card, Input, Form  } from 'antd'
import  {  UploadOutlined  } from '@ant-design/icons';
import { useState } from 'react';
import {connect} from "react-redux";

const UploadUrl = (props) =>{
    const [isUploading, setIsUploading] = useState(false)
    const onFinish = (values) => {
        let myformData= new FormData()
        myformData.append('url',values.url)
        setIsUploading(true)
        fetch('http://localhost:5000/uploadurl',{
            method: 'post',
            body: myformData
        }).then(res => res.json())
            .then(()=>{
                console.log('upload succeeded');
            }).catch(()=>{
                message.error('upload failed')
        }).finally(()=>{
            setIsUploading(false);
        })
        console.log('Success:', values.url);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <Form
            name="basic"
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}

            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >
            <Form.Item
                label="Url"
                name="url"
                rules={[
                    {
                        required: true,
                        message: 'Enter url sample',
                    },
                ]}
            >
                <Input />
            </Form.Item>


            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    )
}


function mapDispatchToProps(dispatch){
    return {
        testFunction: () => dispatch({type: "URL_SUBMIT"}),
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(UploadUrl);