import {connect} from "react-redux";
import React,{Component} from 'react';
import {Table,Tag,Space, Card, Row, Col} from 'antd';

/*Get a list of uploaded files */

class Products  extends Component {
    constructor(props){
        super(props);
        this.state = {
            items:[],
            errors:[]
        }
        console.log(this.state.items)
    }
    componentDidMount() {
        let data_url = "http://localhost:5000/productslist"

        fetch(data_url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        items: result
                    })
                },
                (error) =>{
                    this.setState({
                        errors:error,
                    })
                })
    }

    render(){

        return (
            <div>
                <Row gutter={16}>
                    <Col span={8}>
                        <Card hoverable>
                            Item 1
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card hoverable>
                            Item 1
                        </Card>
                    </Col>
                    <Col span={8}>
                        <Card hoverable>
                            Item 1
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}


function mapDispatchToProps(dispatch){
    return {
        filelistrecord: (curRec) => dispatch({type: "USER_INTERACTION", record: curRec})
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(Products);
