import {  Upload, message, Button, Card  } from 'antd'
import  {  UploadOutlined  } from '@ant-design/icons';
import { useState } from 'react';
import {connect} from "react-redux";



const UploadFile = () => {

    const handleUpload = () =>{
        const formData = new FormData();
        /*fileList.map(file => {
          formData.append('files[]', file);
        });
        setIsUploading(true);
        fetch('http://localhost:5000/upload', {
        method: 'POST',
        body: formData,
      })
        .then(res => res.json())
        .then(() => {
          setFileList({fileList: []})
          message.success('upload successfully.');
        })
        .catch(() => {
          message.error('upload failed.');
        })
        .finally(() => {
          setIsUploading(false)
        });*/
    }
    const props = {
        action: 'http://localhost:5000/uploadfile',
        multiple: true,
        onChange({ file, fileList }) {
            console.log('-----'+file.status)
            if (file.status !== 'uploading') {
                console.log(file, fileList);
            }
        },
        beforeUpload(singlefile,filelist){
           console.log("submited file list:"+filelist);
        }
    }

    return (
        <Card>
            <Upload {...props} >
                <Button icon={<UploadOutlined />}
                        onClick={handleUpload}>

                    Click to Upload
                </Button>
            </Upload>
        </Card>
    )
}

function mapDispatchToProps(dispatch){
    return {
        menuClicked: () => dispatch({type: "USER_INTERACTION"}),
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(UploadFile);
