import React from 'react';
import {  Upload, message, Button, Card  } from 'antd'
import  {  UploadOutlined  } from '@ant-design/icons';
import { useState } from 'react';
import {connect} from "react-redux";
import UploadFile from "./UploadFile";
import UploadUrl from "./UploadUrl";



const Uploads = () => {



    return (
        <React.Fragment>
            <Card>
                <p>Upload files</p>
                <UploadFile />
            </Card>
            <Card>
                <p>Enter url to analyze</p>
                <UploadUrl />
            </Card>
        </React.Fragment>
    )
}

function mapDispatchToProps(dispatch){
    return {
        menuClicked: () => dispatch({type: "USER_INTERACTION"}),
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(Uploads);
