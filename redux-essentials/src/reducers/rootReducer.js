import {combineReducers} from 'redux';
import navigationReducer from './navigationSlice'
import navReducer from "./navReducer";
const rootReducer =  combineReducers({
    interactions: navReducer,
});

export default rootReducer;