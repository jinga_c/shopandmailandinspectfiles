import {createSlice} from '@reduxjs/toolkit';


const initialState= {
    menuActive: 0,
}

const navigationSlice = createSlice({
    name: 'navigation',
    initialState,
    reducers: {}

})

export default navigationSlice.reducer;