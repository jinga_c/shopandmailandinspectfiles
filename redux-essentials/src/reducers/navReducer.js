const initialState={
    number: 0
}
const  navReducer = (state=initialState,action) => {

    switch(action.type){
        case 'USER_INTERACTION':
            let increment = state.number + 1;

            console.log(increment);
            return Object.assign({}, state, {
                number: increment
            })
        default:
            return state;
    }
}
export default navReducer
