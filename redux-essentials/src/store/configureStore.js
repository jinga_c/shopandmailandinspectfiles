import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import {navReducer} from "../reducers/navReducer";
import monitorReducersEnhancer from '../enhancers/monitorReducers'
import loggerMiddleware from '../middleware/logger'
import rootReducer from '../reducers/rootReducer'
import {configureStore} from "@reduxjs/toolkit";

export default function configureAppStore(preloadedState) {
  const middlewares = [loggerMiddleware, thunkMiddleware]
  const middlewareEnhancer = applyMiddleware(...middlewares)

  const enhancers = [middlewareEnhancer, monitorReducersEnhancer]
  const composedEnhancers = composeWithDevTools(...enhancers)

  const store = configureStore({
    reducer: rootReducer,
    middleware: middlewares,
    enhancers: enhancers,

  })



  return store
}