import {connect} from "react-redux";
import React from 'react';
import { Menu } from 'antd';
import { MailOutlined, AppstoreOutlined, ContainerOutlined, CalendarOutlined,UploadOutlined, GiftOutlined} from '@ant-design/icons';
import {Link,Route, Routes, Router} from 'react-router-dom';
const {Submenu} = Menu
class NavMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            menuActive: '',
        }
    }
    componentDidMount() {
        console.log(this.props.interactions)
    }
    render() {
        return (
            <Menu
                defaultOpenKeys={['1']}
                defaultSelectedKeys={['1']}
                mode="inline"
                theme="light"
            >
                <Menu.Item key="1"><Link to="/fileslist" ><ContainerOutlined style={{width: '70px'}} /> Files list</Link></Menu.Item>
                <Menu.Item key="2"><Link to="/statistics"><AppstoreOutlined style={{width: '70px'}} /> Statistics</Link></Menu.Item>
                <Menu.Item key="3"><Link to=""><MailOutlined style={{width: '70px'}} /> Mails</Link></Menu.Item>
                <Menu.Item key="4"><Link to="/products"><GiftOutlined   style={{width: '70px'}} /> Shop </Link></Menu.Item>
                <Menu.Item key="5"><Link to="/upload"><UploadOutlined  style={{width: '70px'}} /> Samples </Link></Menu.Item>

            </Menu>
        )
    }
}

function mapDispatchToProps(dispatch){
    return {
        menuClicked: () => dispatch({type: "USER_INTERACTION"}),
    }
}
function mapStateToProps(state){
    return {
        interactions: state.interactions
    }
}
export default connect (
    mapStateToProps,
    mapDispatchToProps,
)(NavMenu);
