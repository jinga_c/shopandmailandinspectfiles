import os

import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SQLALCHEMY_DATABASE_URI = 'postgresql://amanage:amanage@localhost:5432/flask_amanage' 
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'DRSm@%J6F*LeX5T9tJTvsgmxcR*Rm6jj+fpwNaA=W*9+abTN'
    DEBUG = False
    MAIL_SERVER='localhost'
    MAIL_PORT=8025
    ADMINS=['admin@localhost.com']
    UPLOAD_FOLDER='/home/yavor/flask-projects/amanage/uploads'
    MAX_CONTENT_LENGTH= 16*1024*1024; #16Mb
    X_APIKEY="95fa9f0c5e2c04ad2998688c9311f048905a6aca3a602d964858251bf1bdf782"
    FILE_UPLOAD_URL="https://www.virustotal.com/api/v3/files"
    ANALYSIS_URL="https://www.virustotal.com/api/v3/analyses"
