import os, pdb
from werkzeug.utils import secure_filename
from config import Config
import requests
from datetime import datetime

class Util(object):


    def formatUploadedFile(uploaded_file):        
        bstring_file = uploaded_file.stream
        mimetype=uploaded_file.mimetype
        bstring=bstring_file.read()
        upl_filename=secure_filename(uploaded_file.filename);
        file_for_upload = {'file':  bstring}
        timestamp = datetime.now().timestamp()
        file_info ={'mimetype':mimetype, 'filename':upl_filename, 'fsz_bytes':len(bstring),'timestamp':timestamp}
        return file_for_upload, file_info;

    def uploadFile(file_for_upload):
        url = Config.FILE_UPLOAD_URL
        headers = {
            'x-apikey': Config.X_APIKEY,
            'Accept': 'application/json',
        }
        sendfile = requests.post(url,files=file_for_upload,headers=headers)
        json_response = sendfile.json()
        operation_type = json_response['data']['type']
        operation_id = json_response['data']['id']
        formated_response = { 'status_code': sendfile.status_code, 
            'op_type': operation_type, 'op_id': operation_id, 'response_text': sendfile.text}
        return formated_response;

    def processResponseFromServer(op_id):
        url = f"{Config.ANALYSIS_URL}/{op_id}"
        headers = {
            'x-apikey': Config.X_APIKEY,
            'Accept': 'application/json',
        }
        response = requests.get(url,headers=headers)        
        formated_response = {'response_code': response.status_code, 'response_text': response.text}
        return response.json();

    def formatResponseFromServer(json_resp):
            sha256=json_resp['meta']['file_info']['sha256']
            sha1=json_resp['meta']['file_info']['sha1']
            md5=json_resp['meta']['file_info']['md5']
            size=json_resp['meta']['file_info']['size']
            date=json_resp['data']['attributes']['date']
            status=json_resp['data']['attributes']['status']
            stats=json_resp['data']['attributes']['stats']
            info_url=json_resp['data']['links']['item']
            response_obj = {'sha256':sha256, 'sha1': sha1, 'md5': md5, 'size': size, 'date': date, 
                    'status':status, 'stats':stats, 'info_url': info_url}
            return response_obj;

    def getResultsForFile(url):
        headers = {
            'x-apikey': Config.X_APIKEY,
            'Accept': 'application/json',
        }
        response = requests.get(url,headers=headers)      
        res = response.json()
        my_response = {"type": res['data']['type'], "obj_id": res['data']['id'], "links": res['data']['links'], "attributes": res['data']['attributes']}               
        return my_response;

    def processResultsForFile(file_resp_obj):
        pdb.set_trace()
        
    def processUploadedUrl():
        return 'test';

