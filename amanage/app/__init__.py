from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import logging
from logging.handlers import SMTPHandler
app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app,db)

from app import routes,models,errors

#if not app.debug:
#    auth = None
#    secure = None
#    mail_handler = SMTPHandler(
#            mailhost = (app.config['MAIL_SERVER'], app.config['MAIL_PORT']), fromaddr='no-reply@'+ app.config['MAIL_SERVER'],
#                toaddrs=app.config['ADMINS'], subject='Amanage error', credentials=auth, secure=secure)
#
#    mail_handler.setLevel(logging.ERROR)
#    app.logger.addHandler(mail_handler)
#
