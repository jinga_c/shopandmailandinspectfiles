from app import app
from flask import render_template, jsonify, request 
import pdb, os, json
from app import db
from flask_cors import cross_origin
import requests
from app.models import UploadFile, UploadedFileOperation, UploadUrl, UploadedUrlOperation, UploadedFileResponse, FileAntivirusResponse
from datetime import datetime
from util import Util

@app.route("/")
def index():
    pass
"""******************************
************ASSETS***************
******************************"""
@app.route("/asset", methods=['POST'])
def asset():
    return "expecting user input"

@app.route("/asset/findBy", methods=['GET'])
def getAsset():
    user = {"method":"GET"}
    return user

@app.route("/vulnerabilities-for-asset", methods=['GET','POST'])
def vulnerabilitiesForAsset():
    pass


"""*********************************
**************ACTiONS***************
*********************************
@app.route("/create-action", methods=['POST'])
def createAction():
    action_name = request.form.get("name")
    action = Action(name=action_name)
    db.session.add(action)
    db.session.commit()
    return jsonify({"200":"ok"})

@app.route("/delete-action", methods=['POST'])
def deleteAction():
    action_name = request.form.get("name")
    action = Action(name=action_name)
    db.session.add(action)
    db.session.commit()
    return jsonify({"200":"ok"})

@app.route("/update-action", methods=['PATCH','PUT'])
def updateAction():
    action_name = request.form.get("name")
    action = Action(name=action_name)
    db.session.add(action)
    db.session.commit()
    return jsonify({"200":"ok"})
"""


"""*********************************
**************SERVICE***************
*********************************"""

@app.route("/service", methods=['GET','POST'])
def service():
    pass

"""*********************************
**************POLICY****************
*********************************"""


@app.route("/policy", methods=['GET','POST'])
def policy():
    pass

"""*********************************
**************LICENSE***************
*********************************"""

@app.route("/license", methods=['GET','POST'])
def license():
    pass



"""*********************************
**************COMPANY***************
*********************************"""
@app.route("/company", methods=['GET','POST'])
def company():
    pass


"""*********************************
**************USER******************
*********************************"""


@app.route("/user", methods=['GET','POST'])
def user():
    pass

"""*********************************
**********VULNERABILITY*************
*********************************"""




@app.route("/vulnerability", methods=['GET','POST'])
def vulnerability():
    pass
"""*******************************************
**************VULNERABILITY-FIX***************
*******************************************"""

@app.route("/vulnerability-fix", methods=['GET','POST'])
def vulnerabilityFix():
    pass

"""*******************************************
***********Upload file ***********************
*******************************************"""
@app.route("/uploadfile", methods=['POST'])
@cross_origin()
def uploadfiles():
    if(request.method == 'POST'):
        #Format the uploaded file for upload        
        file_for_upload, file_info = Util.formatUploadedFile(request.files['file'])
        #Save uploaded file information to db
        file_upl = UploadFile(name=file_info['filename'], mimetype=file_info['mimetype'], 
            fsz_bytes=file_info['fsz_bytes'], upload_date=file_info['timestamp'])

        #Send the uploaded file for inspection
        file_send_for_inspection = Util.uploadFile(file_for_upload)

        #Save the results in db                
        file_upl_op = UploadedFileOperation(op_type=file_send_for_inspection['op_type'], 
                op_id=file_send_for_inspection['op_id'])
        file_upl.operations=[file_upl_op];


        resp_from_server = Util.processResponseFromServer(file_send_for_inspection['op_id']);
        process_response = Util.formatResponseFromServer(resp_from_server)

        upl_file_resp = UploadedFileResponse(sha256=process_response['sha256'], sha1=process_response['sha1'],
            md5=process_response['md5'], size=process_response['size'], date=process_response['date'], 
            status=process_response['status'],stats=json.dumps(process_response['stats']), 
            info_url=process_response['info_url'])
        file_upl.submitted_file = [upl_file_resp]    
        #Get the data from the file results url 
        results_for_file = Util.getResultsForFile(process_response['info_url']);
        #Process the data 
        antiv_results = FileAntivirusResponse(attributes=results_for_file['attributes'], 
                obj_type=results_for_file['type'], obj_id=results_for_file['obj_id'], links=results_for_file['links'], 
                )
        file_upl.antiv_response = [antiv_results]
        db.session.add(file_upl)
        db.session.commit();       
        #Format and save response 
        
        #request complete
        #When requests is complete get the inspection result of the uploaded file and write it to db and then format it and output it to 
        #the user
        return jsonify({file_send_for_inspection['status_code']: file_send_for_inspection['response_text']})
    else:
        return jsonify({"502":"Internal server error"});


"""********************************
**********Upload a url*************
********************************"""
@app.route("/uploadurl", methods=['POST'])
@cross_origin()
def uploadurl():
    if(request.method == 'POST'):
        sampleurl = request.form.get('url')
        data = {'url': sampleurl}
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'x-apikey':'95fa9f0c5e2c04ad2998688c9311f048905a6aca3a602d964858251bf1bdf782'
            }
        url = "https://www.virustotal.com/api/v3/urls";
        sendfile = requests.post(url,data=data,headers=headers)
        json_response = sendfile.json()
        operation_type = json_response['data']['type']
        operation_id = json_response['data']['id']
        upload_url = UploadUrl(name=sampleurl);
        upload_url_op = UploadedUrlOperation(op_type=operation_type, op_id=operation_id)
        upload_url.url_operations = [upload_url_op]
        db.session.add(upload_url);
        db.session.commit();

        #request complete
        return jsonify({sendfile.status_code: sendfile.text})
    else:
        return jsonify({"502":"Internal server error"});
"""*************************
****getFileList()
Method to return an array of uploaded file objects;
TODO: think about pagination
*************************"""

@app.route("/getfilelist", methods=["GET"])
def getFileList():
    #uploaded_file = UploadFile.query.filter_by(id=15).first(); # query.all()
    uploaded_file = UploadFile.query.all(); # query.all()
    upl_fl_data = []
    upl_fl_operations = []
    upl_fl_submitted_file  = []
    upl_fl_antiv_response = []
    new_struct = []
    
    for i in uploaded_file:
        upl_fl_data.append(i.to_dict(show=['antiv_response', 'submitted_file', 'operations' ]))        
    return jsonify(upl_fl_data);
    


@app.route("/getfileresult", methods=["GET"])
def getResult(type,op_id):
    pass


##TODO Save information about the file

@app.route("/parserawresponse", methods=["POST"])
def parseRawResponse(rawresponse):
    pdb.set_trace()
    return False     