from app import db
from sqlalchemy.sql import func
from sqlalchemy.orm.attributes import QueryableAttribute
from sqlalchemy.dialects.postgresql import JSON, JSONB

# Create your models here.

class BaseModel(db.Model):
    __abstract__ = True

    def __repr__(self):
        return "<{}({})>".format(
            self.__class__.__name__,
            ', '.join(
                ["{}={}".format(k, repr(self.__dict__[k]))
                    for k in sorted(self.__dict__.keys())
                    if k[0] != '_']
            )
        )
    def as_dict(self):
        return { c.name: getattr(self, c.name) for c in self.__table__.columns}
    def to_dict(self, show=None, _hide=None, _path=None):
        """Return a dictionary representation of this model."""

        show = show or []
        _hide = _hide or []

        hidden = self._hidden_fields if hasattr(self, "_hidden_fields") else []
        default = self._default_fields if hasattr(self, "_default_fields") else []
        default.extend(['id', 'modified_at', 'created_at'])

        if not _path:
            _path = self.__tablename__.lower()

            def prepend_path(item):
                item = item.lower()
                if item.split(".", 1)[0] == _path:
                    return item
                if len(item) == 0:
                    return item
                if item[0] != ".":
                    item = ".%s" % item
                item = "%s%s" % (_path, item)
                return item

            _hide[:] = [prepend_path(x) for x in _hide]
            show[:] = [prepend_path(x) for x in show]

        columns = self.__table__.columns.keys()
        relationships = self.__mapper__.relationships.keys()
        properties = dir(self)

        ret_data = {}

        for key in columns:
            if key.startswith("_"):
                continue
            check = "%s.%s" % (_path, key)
            if check in _hide or key in hidden:
                continue
            if check in show or key in default:
                ret_data[key] = getattr(self, key)

        for key in relationships:
            if key.startswith("_"):
                continue
            check = "%s.%s" % (_path, key)
            if check in _hide or key in hidden:
                continue
            if check in show or key in default:
                _hide.append(check)
                is_list = self.__mapper__.relationships[key].uselist
                if is_list:
                    items = getattr(self, key)
                    if self.__mapper__.relationships[key].query_class is not None:
                        if hasattr(items, "all"):
                            items = items.all()
                    ret_data[key] = []
                    for item in items:
                        ret_data[key].append(
                            item.to_dict(
                                show=list(show),
                                _hide=list(_hide),
                                _path=("%s.%s" % (_path, key.lower())),
                            )
                        )
                else:
                    if (
                        self.__mapper__.relationships[key].query_class is not None
                        or self.__mapper__.relationships[key].instrument_class
                        is not None
                    ):
                        item = getattr(self, key)
                        if item is not None:
                            ret_data[key] = item.to_dict(
                                show=list(show),
                                _hide=list(_hide),
                                _path=("%s.%s" % (_path, key.lower())),
                            )
                        else:
                            ret_data[key] = None
                    else:
                        ret_data[key] = getattr(self, key)

        for key in list(set(properties) - set(columns) - set(relationships)):
            if key.startswith("_"):
                continue
            if not hasattr(self.__class__, key):
                continue
            attr = getattr(self.__class__, key)
            if not (isinstance(attr, property) or isinstance(attr, QueryableAttribute)):
                continue
            check = "%s.%s" % (_path, key)
            if check in _hide or key in hidden:
                continue
            if check in show or key in default:
                val = getattr(self, key)
                if hasattr(val, "to_dict"):
                    ret_data[key] = val.to_dict(
                        show=list(show),
                        _hide=list(_hide),
                        _path=('%s.%s' % (_path, key.lower())),
                    )
                else:
                    try:
                        ret_data[key] = json.loads(json.dumps(val))
                    except:
                        pass

        return ret_data

class User(BaseModel):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50),index=True, unique=True, nullable=False)
    password = db.Column(db.String(100), index=True, unique=True,  nullable=False)
    email = db.Column(db.String(100), nullable=False)
    _default_fields = [
        "username",
        "password",
        "email",
    ]



class UploadFile(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), index=True, nullable=False) 
    mimetype = db.Column(db.String(100), index=True, nullable=False) 
    fsz_bytes = db.Column(db.Integer, nullable=False) 
    upload_date = db.Column(db.String, nullable=False)
    operations = db.relationship('UploadedFileOperation', backref='uplfileop', lazy=True)
    submitted_file = db.relationship('UploadedFileResponse', backref='uplfileresp', lazy=True)
    antiv_response = db.relationship('FileAntivirusResponse', backref='fileantvresp', lazy=True)

    _default_fields = [
        "name",
        "mimetype",
        "submitted_file",
        "fsz_bytes",
        "upload_date",
        "operations"
        "antiv_response",
    ]


class UploadedFileOperation(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    op_type = db.Column(db.String(50), index=True, nullable=False) 
    op_id = db.Column(db.String(100), index=True, nullable=False) 
    file_id = db.Column(db.Integer, db.ForeignKey('upload_file.id'))
    _default_fields = [
        "op_type",
        "op_id",
    ]

class UploadedFileResponse(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    sha256=db.Column(db.String(64), index=True, nullable=False) 
    sha1=db.Column(db.String(40), index=True, nullable=False) 
    md5=db.Column(db.String(32), index=True, nullable=False) 
    size=db.Column(db.Integer, index=True, nullable=False) 
    date=db.Column(db.Integer, index=True, nullable=False) 
    status=db.Column(db.String(10), index=True, nullable=False) 
    stats=db.Column(db.String(250), index=True, nullable=False) 
    info_url=db.Column(db.String(150), index=True, nullable=False) 
    file_id = db.Column(db.Integer, db.ForeignKey('upload_file.id'))

    _default_fields = [
        "sha256",
        "sha1",
        "md5",
        "size",
        "date",
        "status",
        "info_url",        
    ]

class FileAntivirusResponse(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    attributes = db.Column(JSON, nullable=False)
    obj_type = db.Column(db.String(50), nullable=False)
    obj_id = db.Column(db.String(255), nullable=False)
    links = db.Column(JSON, nullable=False)
    file_id = db.Column(db.Integer, db.ForeignKey('upload_file.id'));
    _default_fields = [
        "attributes",
        "obj_type",
        "obj_id",
        "links",
    ]

class UploadUrl(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), index=True, nullable=False) 
    url_operations = db.relationship('UploadedUrlOperation', backref='uplurlop', lazy=True)
    _default_fields = [
        "name",
    ]

class UploadedUrlOperation(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    op_type = db.Column(db.String(50), index=True, nullable=False) 
    op_id = db.Column(db.String(100), index=True, nullable=False) 
    url_id = db.Column(db.Integer, db.ForeignKey('upload_url.id'))
    _default_fields = [
        "op_type",
        "op_id",
    ]

class Product(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), index=True, nullable=False) 
    description = db.Column(db.String(255), index=True, nullable=False) 
    price = db.Column(db.Integer, index=True, nullable=False)
    image = db.Column(db.LargeBinary, nullable=False) 
    comments = db.relationship('ProductComments', backref='productcomments', lazy=True) 
    rating = db.relationship("ProductRating", backref='productrating', lazy=True)

class ProductComments(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))


class ProductRating(BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))



