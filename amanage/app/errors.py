from flask import render_template,jsonify
from app import app, db

@app.errorhandler(404)
def not_found_error(error):
    response = {"message":"resource not found"}
    return jsonify(response)

@app.errorhandler(500)
def internal_error(error):
    response = {"message":"internal server error, the administrator will be notified. Sorry for the incovinience"}
    return jsonify(response)
